import dayjs from "@/common/dayjs.js"


//新消息，不显示
//当天消息，显示：10:22
//昨天消息，显示：昨天 20:41
//今年消息，上午下午，显示：3月17日 下午16:45
//其他消息，上午下午，显示：2020年11月2日 下午15:17

/*不显示的条件*/
// 新消息（小于5分钟）
// 当天的消息（间隔不大于等于5分钟） 

//循环处理时间
/**
 * 参数
 * arr：数据数组
 * key：数组中对象的时间key键。
 * 新增属性
 * show_time_type：时间的类型
 * show_time：页面展示输出的时间
 * is_show_time：间隔上个时间是否大于5分钟，大于则显示当前时间，反之。
 **/
export function checkShowRule(arr : any, key : any) {
	var newArr = arr.map((item : any, index : any, array : any) => {
		var obj = toggleTime(item[key]);
		item['show_time_type'] = obj.type;
		item['show_time'] = obj.time;
		if (index > 0) {
			item['is_show_time'] = compareTimeInterval(array[index - 1][key], array[index][key]);
		} else {
			item['is_show_time'] = true;
		}
		return item;
	});
	// console.log(newArr, 'newArr')
	return newArr;
}
//根据不同时间的消息，输出不同的时间格式
function toggleTime(date : string) {
	var time;
	var type = getDateDiff(date);
	//1：新消息，2：当天消息,3：昨天消息，4：今年消息，5：其他消息
	if (type == 1) {
		// time = "以下为最新消息";//新消息，不显示时间，但是要显示"以下为最新消息"
		time = dayjs(date).format("H:mm");
	} else if (type == 2) {
		time = dayjs(date).format("H:mm");//当天消息，显示：10:22
	} else if (type == 3) {
		time = dayjs(date).format("昨天 H:mm");//昨天消息，显示：昨天 20:41
	} else if (type == 4) {
		time = dayjs(date).format("M月D日 AH:mm").replace("AM", "上午").replace("PM", "下午");//今年消息，上午下午，显示：3月17日 下午16:45
	} else if (type == 5) {
		time = dayjs(date).format("YYYY年M月D日 AH:mm").replace("AM", "上午").replace("PM", "下午");//其他消息，上午下午，显示：2020年11月2日 下午15:17
	}
	return {
		time: time,
		type: type
	};
}
//判断消息类型
function getDateDiff(date : string) {
	var nowDate = dayjs(new Date());//当前时间
	var oldDate = dayjs(new Date(date));//参数时间
	var result;
	if (nowDate.year() - oldDate.year() >= 1) {
		result = 5;
	} else if (nowDate.month() - oldDate.month() >= 1 || nowDate.date() - oldDate.date() >= 2) {
		result = 4;
	} else if (nowDate.date() - oldDate.date() >= 1) {
		result = 3;
	} else if (nowDate.hour() - oldDate.hour() >= 1 || nowDate.minute() - oldDate.minute() >= 5) {
		result = 2;
	} else {
		result = 1;
	}
	return result;
}

//判断两个时间差是否大于5分钟
function compareTimeInterval(t1 : string, t2 : string) {
	// console.log(t1,t2,dayjs(t2)-dayjs(t1));
	return dayjs(t2) - dayjs(t1) >= 300000 ? true : false;
}