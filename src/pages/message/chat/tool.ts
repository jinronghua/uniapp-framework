
// 选择图片
export const chooseImage = () => {
	return new Promise((resolve, reject) => {

		uni.chooseImage({
			count: 1, //默认9
			type: 'image',
			success: (res : any) => {
				const tempFilePaths = res.tempFiles;
				let filePath = 'file:///' + plus.io.convertLocalFileSystemURL(res.tempFilePaths[0]);
				const data = {
					url: filePath,
					filePath
				}
				resolve(data);

			},
			fail: (err) => {
				console.log(err, 'err');
				reject(err);
			}
		});
	})
}