import httpUrl from '@/config'
import { getToken } from '@/utils/token'
import { errCode } from '@/utils/errorCode'
import { toast } from '@/utils/common'

let timeout = 10000
const baseUrl = httpUrl.baseUrl

const upload = config => {
	// 是否需要设置 token
	const isToken = (config.headers || {}).isToken === false
	config.header = config.header || {}
	if (getToken() && !isToken) {
		config.header['Authorization'] = 'Bearer ' + getToken()
	}
	return new Promise((resolve, reject) => {
		uni.uploadFile({
			timeout: config.timeout || timeout,
			url: baseUrl + config.url,
			filePath: config.filePath,
			name: config.name || 'file',
			header: config.header,
			formData: config.formData,
			success: (res) => {
				let result = JSON.parse(res.data)
				const code = result.code || 200
				if (code === 200) {
					resolve(result)
				} else if (code !== 200 && code != 500) {
					errCode(code)
					reject(code)
				} else if (code === 500) {
					toast(msg)
					reject('500')
				}
			},
			fail: (error) => {
				let message = error.message
				if (message == 'Network Error') {
					message = '后端接口连接异常'
				} else if (message.includes('timeout')) {
					message = '系统接口请求超时'
				} else if (message.includes('Request failed with status code')) {
					message = '系统接口' + message.substr(message.length - 3) + '异常'
				}
				toast(message)
				reject(error)
			}
		})
	})
}

export default upload