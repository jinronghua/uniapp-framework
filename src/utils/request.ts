import httpUrl from '@/config'
import { getToken } from '@/utils/token'
import { errCode } from '@/utils/errorCode'
import { tansParams, toast } from '@/utils/common'

// 请求参数
interface RequestConfig {
	url : string;
	method ?: any;
	timeout ?: number;
	baseUrl ?: string;
	data ?: any;
	header ?: any;
	params ?: any;
	headers ?: {
		isToken ?: boolean;
	};
}

let timeout = 10000
const baseUrl = httpUrl.baseUrl

const request = (config : RequestConfig) => {
	// 是否需要设置 token
	const isToken = (config.headers || {}).isToken === false
	config.header = config.header || {}
	if (getToken() && !isToken) {
		config.header['Authorization'] = 'Bearer ' + getToken()
	}
	// get请求映射params参数
	if (config.params) {
		let url = config.url + '?' + tansParams(config.params)
		config.url = url
	}
	return new Promise((resolve, reject) => {
		uni.request({
			method: config.method || 'get',
			timeout: config.timeout || timeout,
			url: config.baseUrl || baseUrl + config.url,
			data: config.data,
			header: config.header,
			dataType: 'json'
		}).then(response => {
			let res = response.data
			const code = (res as AnyObject).code || 200;
			if (code != 200 && code != 500) {
				errCode(code)
				reject(code)
			} else if (code == 500) {
				uni.showToast({
					title: (res as AnyObject).msg || '系统错误，请联系管理员',
					icon: 'none',
					position: 'top'
				});
				reject(code)
			}
			resolve(res)
		})
			.catch(error => {
				let message = error.errMsg
				if (message === 'Network Error') {
					message = '后端接口连接异常'
				} else if (message.includes('timeout')) {
					message = '系统接口请求超时'
				} else if (message.includes('Request failed with status code')) {
					message = '系统接口' + message.substr(message.length - 3) + '异常'
				}
				toast(message)
				reject(error)
			})
	})
}

export default request