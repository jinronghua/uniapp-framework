import { getCurrentInstance } from 'vue'
import CEventBean from './CEventBean'
import { onUnmounted } from 'vue'

/**
 * 全局事件管理
 */
export default class CEvent {
    private static eventbean = new CEventBean()

    /**
     * 注册
     * @param key 注册名称
     * @param callback 执行回调
     * @param target 附带信息
     * @returns 返回唯一注册id
     */
    static on(key: any, callback: Function, ...target: any) {
        const onid = CEvent.eventbean.on(key, callback, ...target)
        const _node = getCurrentInstance()
        if (_node) {
            onUnmounted(() => {
                CEvent.off(onid)
            })
        }
        return onid
    }

    /**
     * 调用
     * @param key 注册名称
     * @param data 回调函数传入参数
     */
    static emit(key: any, data?: any) {
        return CEvent.eventbean.emit(key, data)
    }

    /**
     * 销毁
     * @mode 模式1:多id或者key名称传入-off('onid1')、off('onid1','getData'...)
     * @mode 模式2:on方法传入的key和方法-off('getData',fun)
     * @param uids-传入的是id或者key加方法清理的是key的单个注册，传入的是key清理key所有注册
     */
    static off(...uids: any) {
        return CEvent.eventbean.off(...uids)
    }

    /**
     * 清空
     */
    static clear() {
        return CEvent.eventbean.clear()
    }
}
