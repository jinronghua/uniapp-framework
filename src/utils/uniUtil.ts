// 预览图片
export const previewpic = (cind : string, clist : Array<string>) => {
	uni.previewImage({
		urls: clist,
		current: cind,
		indicator: 'default',
		loop: false
	});
}

//获取节点信息
export const createSelectorQuery = (View : string) => {
	return new Promise((resolve) => {
		const querry = uni.createSelectorQuery().in(this)
		querry.select(View).boundingClientRect((data : any) => {
			// console.log("得到布局位置信息" + JSON.stringify(data));
			resolve(data);

		}).exec()
	})
}