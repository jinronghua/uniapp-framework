// 日期格式化
export function parseTime(time : any, pattern : string = '') {
	if (arguments.length === 0 || !time) {
		return null
	}
	const format = pattern || '{y}-{m}-{d} {h}:{i}:{s}'
	let date
	if (typeof time === 'object') {
		date = time
	} else {
		if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
			time = parseInt(time)
		} else if (typeof time === 'string') {
			time = time.replace(new RegExp(/-/gm), '/').replace('T', ' ').replace(new RegExp(/\.[\d]{3}/gm), '');
		}
		if ((typeof time === 'number') && (time.toString().length === 10)) {
			time = time * 1000
		}
		date = new Date(time)
	}
	const formatObj : any = {
		y: date.getFullYear(),
		m: date.getMonth() + 1,
		d: date.getDate(),
		h: date.getHours(),
		i: date.getMinutes(),
		s: date.getSeconds(),
		a: date.getDay()
	}
	const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
		let value = formatObj[key]
		// Note: getDay() returns 0 on Sunday
		if (key === 'a') {
			return ['日', '一', '二', '三', '四', '五', '六'][value]
		}
		if (result.length > 0 && value < 10) {
			value = '0' + value
		}
		return value || 0
	})
	return time_str
}
/**
 * 日期对比
 * @param {string} strTime  开始日期
 * @param {string} endTime  结束日期
 * @return 
 */
export function compareDate(strTime : string, endTime : string) {

	var d1 = new Date(Date.parse(strTime.replace(/-/g, "/")));
	var d2 = new Date(Date.parse(endTime.replace(/-/g, "/")));
	if (d1.getTime() <= d2.getTime()) {
		//代码块
		return false;
	} else {
		//代码块
		return true;
	}
}



export function today() {
	const end = new Date();
	const start = new Date();
	return [parseTime(start, '{y}-{m}-{d} 00:00:00'), parseTime(end, '{y}-{m}-{d} 23:59:59')]
}
export function yesterday() {
	const end = new Date();
	const start = new Date();
	start.setTime(start.getTime() - 3600 * 1000 * 24 * 1);
	end.setTime(end.getTime() - 3600 * 1000 * 24 * 1);
	return [parseTime(start, '{y}-{m}-{d} 00:00:00'), parseTime(end, '{y}-{m}-{d} 23:59:59')]
}
export function days7() {
	const end = new Date();
	const start = new Date();
	start.setTime(start.getTime() - 3600 * 1000 * 24 * (7 - 1));
	return [parseTime(start, '{y}-{m}-{d} 00:00:00'), parseTime(end, '{y}-{m}-{d} 23:59:59')]
}
export function days30() {
	const end = new Date();
	const start = new Date();
	start.setTime(start.getTime() - 3600 * 1000 * 24 * (30 - 1));
	return [parseTime(start, '{y}-{m}-{d} 00:00:00'), parseTime(end, '{y}-{m}-{d} 23:59:59')]
}


export function nowMonth() {
	let date = new Date();
	let start = new Date();
	let end = new Date();
	const month = date.getMonth + 1;
	date.setMonth(date.getMonth() + 1); // 先设置为下个月
	date.setDate(0); // 再置0，变成当前月最后一天
	// console.log(date.getDate()); // 当前月最后一天即当前月拥有的天数
	start.setDate(1)
	end.setDate(date.getDate())
	return [parseTime(start, '{y}-{m}-{d} 00:00:00'), parseTime(end, '{y}-{m}-{d} 23:59:59')]
}
// 获取上一个月时间
export const getLastMonth = (value = null, separate = '-') => {
	// 如果为null,则格式化当前时间
	if (!value) value = Number(new Date());
	// 如果dateTime长度为10或者13，则为秒和毫秒的时间戳，如果超过13位，则为其他的时间格式
	if (value.toString().length == 10) value *= 1000;
	value = +new Date(Number(value));

	// 获取上个月时间
	const targetTime = new Date(value);
	let year = targetTime.getFullYear();
	let month = targetTime.getMonth();
	if (month === 0) {
		month = 12;
		year = year - 1;
	}
	if (month < 10) {
		month = '0' + month;
	}
	const yDate = new Date(year, month, 0);
	const startDateTime = year + separate + month + separate + '01 00:00:00'; //上个月第一天
	const endDateTime = year + separate + month + separate + yDate.getDate() + ' 23:59:59'; //上个月最后一天
	return [startDateTime, endDateTime];
};

//获取本周七天时间列表
export function getWeekList(dateString : string) {
	let dateStringReg = /^\d{4}[/-]\d{1,2}[/-]\d{1,2}$/;
	if (dateString.match(dateStringReg)) {
		let presentDate = new Date(dateString),
			today = presentDate.getDay() !== 0 ? presentDate.getDay() : 7;
		return Array.from(new Array(7), function (val, index) {
			return parseTime(new Date(presentDate.getTime() - (today - index - 1) * 24 * 60 * 60 *
				1000), "{y}-{m}-{d}");
		});
	} else {
		throw new Error('dateString should be like "yyyy-mm-dd" or "yyyy/mm/dd"');
	}
}

// 计算指定时间是星期几(中文)
export function getweekday(date : any) {
	// date例如:'2022-03-05'
	const weekArray = new Array("日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六")
	const week = weekArray[new Date(date).getDay()]
	// 例如返回:'星期一'
	return week
}

// 计算指定时间是星期几(数字)
export function getweekday_num(date : any) {
	// date例如:'2022-03-05'
	const obj : any = { '星期一': 1, '星期二': 2, '星期三': 3, '星期四': 4, '星期五': 5, '星期六': 6, '星期日': 7 }
	const weekArray = new Array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六")
	const week : any = weekArray[new Date(date).getDay()]
	// 例如返回:'1'
	return obj[week]
}


// 聊天内容时间
export const formatCXTMsgTime = (timespan : number) => {
	var dateTime = new Date(timespan);

	var year = dateTime.getFullYear();
	var month : string = (dateTime.getMonth() + 1).toString();
	var day : string = dateTime.getDate().toString();
	var hour : string = dateTime.getHours().toString();
	var minute : string = dateTime.getMinutes().toString();
	var second : string = dateTime.getSeconds().toString();
	var week : string = dateTime.getDay().toString();
	var now = new Date();
	var now_new = Date.parse(now.toDateString());  //typescript转换写法
	month = Number(month) < 10 ? 0 + '' + month : month;
	day = Number(day) < 10 ? 0 + '' + day : day;
	hour = Number(hour) < 10 ? 0 + '' + hour : hour;
	minute = Number(minute) < 10 ? 0 + '' + minute : minute;
	second = Number(second) < 10 ? 0 + '' + second : second;

	var milliseconds = 0;
	var timeSpanStr;

	milliseconds = now_new - timespan;

	if (Number(day) == now.getDate()) {
		timeSpanStr = "今天" + hour + ":" + minute;
	}
	else if (Number(day) == now.getDate() - 1) {
		timeSpanStr = "昨天" + hour + ":" + minute;
	}
	else if (year == now.getFullYear()) {
		timeSpanStr = month + '-' + day + ' ' + hour + ':' + minute;
	} else {
		timeSpanStr = year + '-' + month + '-' + day + ' ' + hour + ':' + minute;
	}
	return timeSpanStr;
};

// 防微信聊天
export const formatWxMsgTime = (timespan : number) => {
	var dateTime = new Date(timespan);

	var year = dateTime.getFullYear();
	var month : string = (dateTime.getMonth() + 1).toString();
	var day : string = dateTime.getDate().toString();
	var hour : string = dateTime.getHours().toString();
	var minute : string = dateTime.getMinutes().toString();
	var second : string = dateTime.getSeconds().toString();
	var now = new Date();
	var now_new = Date.parse(now.toDateString());  //typescript转换写法
	month = Number(month) < 10 ? 0 + '' + month : month;
	day = Number(day) < 10 ? 0 + '' + day : day;
	hour = Number(hour) < 10 ? 0 + '' + hour : hour;
	minute = Number(minute) < 10 ? 0 + '' + minute : minute;
	second = Number(second) < 10 ? 0 + '' + second : second;
	var milliseconds = 0;
	var timeSpanStr;

	milliseconds = now_new - timespan;

	if (Number(day) == now.getDate()) {
		timeSpanStr = hour + ':' + minute;
	}
	else if (Number(day) == now.getDate() - 1) {
		timeSpanStr = "昨天";
	}
	else if (year == now.getFullYear()) {
		timeSpanStr = month + '-' + day + ' ' + hour + ':' + minute;
	} else {
		timeSpanStr = year + '-' + month + '-' + day + ' ' + hour + ':' + minute;
	}
	return timeSpanStr;
};

export const formatMsgTime = (timespan : number) => {
	var dateTime = new Date(timespan);

	var year = dateTime.getFullYear();
	var month : string = (dateTime.getMonth() + 1).toString();
	var day : string = dateTime.getDate().toString();
	var hour : string = dateTime.getHours().toString();
	var minute : string = dateTime.getMinutes().toString();
	var second : string = dateTime.getSeconds().toString();
	var now = new Date();
	var now_new = Date.parse(now.toDateString());  //typescript转换写法
	month = Number(month) < 10 ? 0 + '' + month : month;
	day = Number(day) < 10 ? 0 + '' + day : day;
	hour = Number(hour) < 10 ? 0 + '' + hour : hour;
	minute = Number(minute) < 10 ? 0 + '' + minute : minute;
	second = Number(second) < 10 ? 0 + '' + second : second;
	var milliseconds = 0;
	var timeSpanStr;

	milliseconds = now_new - timespan;

	if (milliseconds <= 1000 * 60 * 1) {
		timeSpanStr = '刚刚';
	}
	else if (1000 * 60 * 1 < milliseconds && milliseconds <= 1000 * 60 * 60) {
		timeSpanStr = Math.round((milliseconds / (1000 * 60))) + '分钟前';
	}
	else if (1000 * 60 * 60 * 1 < milliseconds && milliseconds <= 1000 * 60 * 60 * 24) {
		timeSpanStr = Math.round(milliseconds / (1000 * 60 * 60)) + '小时前';
	}
	else if (1000 * 60 * 60 * 24 < milliseconds && milliseconds <= 1000 * 60 * 60 * 24 * 15) {
		timeSpanStr = Math.round(milliseconds / (1000 * 60 * 60 * 24)) + '天前';
	}
	else if (milliseconds > 1000 * 60 * 60 * 24 * 15 && year == now.getFullYear()) {
		timeSpanStr = month + '-' + day + ' ' + hour + ':' + minute;
	} else {
		timeSpanStr = year + '-' + month + '-' + day + ' ' + hour + ':' + minute;
	}
	return timeSpanStr;
};

export const timeAgo = (date : any) => {
	var seconds = Math.floor((new Date() - date) / 1000);
	var interval = Math.floor(seconds / 31536000);

	if (interval > 1) {
		return interval + " 年前";
	}
	interval = Math.floor(seconds / 2592000);
	if (interval > 1) {
		return interval + " 个月前";
	}
	interval = Math.floor(seconds / 86400);
	if (interval > 1) {
		return interval + " 天前";
	}
	interval = Math.floor(seconds / 3600);
	if (interval > 1) {
		return interval + " 小时前";
	}
	interval = Math.floor(seconds / 60);
	if (interval > 1) {
		return interval + " 分钟前";
	}
	return Math.floor(seconds) + " 秒前";
};

export const stringToDate = (dateString : string) => {
	return new Date(Date.parse(dateString)).getTime();
}