type BusClass = {
	emit : (name : string) => void,
	on : (name : string, callback : Function) => void
}

type Pramskey = string | number | symbol;

type List = {
	[key : Pramskey] : Array<Function>
}



class Bus implements BusClass {
	list : List
	constructor() {
		this.list = {}
	}
	emit(name : string, ...args : Array<any>) {
		let evenentName : Array<Function> = this.list[name];
		evenentName.forEach(fn => {
			fn.apply(this, args)
		})

	}
	on(name : string, callback : Function) {
		let fn : Array<Function> = this.list[name] || [];
		fn.push(callback);
		this.list[name] = fn;
	}
}

export default new Bus();