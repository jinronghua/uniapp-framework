let baseUrl : string;

if (process.env.NODE_ENV === 'production') {
	// 生产环境
	baseUrl = '';
} else if (process.env.NODE_ENV === 'development') {
	// 开发环境
	baseUrl = 'http://192.168.1.106:1885/adm';
} else {
	// 其他环境
	baseUrl = 'https://api.local.com';
}
// 融云
let appKey = "p5tvi9dspa3v4";
// 消息推送
const pushOptions = {
	idMI: 'xxx',
	appKeyMI: 'p5tvi9dspa3v4',
	appIdMeizu: 'xxx',
	appKeyMeizu: 'xxx',
	appKeyOPPO: 'xxx',
	appSecretOPPO: 'gQCJ5s6nuM',
	enableHWPush: true,
	enableVIVOPush: true
}
// 应用全局配置
const config = {
	baseUrl,
	pushOptions,
	appKey
};
export default config;