interface IUserInfo {
	openId ?: string // 唯一属性
	id ?: number // 主键
	createdAt ?: string // 创建时间
	updatedAt ?: string // 修改时间
	username : string // 登录名称
	password ?: string // 密码
	email ?: string // 邮箱
	mobile ?: string // 手机
	nickName ?: string // 联系人姓名
	sex ?: string // 性别
	avatarUrl ?: string // 头像
	description ?: string // 描述
	status ?: string // 状态
	lastLoginTime ?: string //
	roleType ?: string // 角色类型
}

interface IUser {
	info : IUserInfo | null
	token : string | null
	expiresIn : number | null

	//以下是登录需要使用的参数
	username : string
	password : string
	loginType : string
}
import { defineStore } from "pinia";

export const useUserStore = defineStore('user', ({
	state: (() => {
		return {
			info: {
				id: "10001",
				nickName: "丸子",
				avatar: "https://img1.baidu.com/it/u=2837426444,1036569200&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1701709200&t=288096b3c48879553176629ea668d09d",
			}, //用户信息 uni.getStorageSync('userInfo')
			token: uni.getStorageSync('token'),  //token
			expiresIn: uni.getStorageSync('expiresIn'), //过期时间
			username: uni.getStorageSync('username'),  //当前用户账号
			password: uni.getStorageSync('password'),  //当前账号密码
			loginType: uni.getStorageSync('loginType') //当前登录类型 001:游客登录 002:短信验证码登录
		}
	}),
	actions: {
		// 登录成功操作
		async loginSuccess(context : any, data : any) : Promise<void> {
			this.setUser(data.userInfo);
			this.setToken(data.token);
			this.setExpires(data.expiresIn);

		},
		async setUser(user : IUserInfo) {
			this.info = user
			uni.setStorageSync('userInfo', user)
		},
		async setToken(token : string) {
			this.token = token
			uni.setStorageSync('token', token)
		},
		async setLoginType(loginType : string) {
			this.loginType = loginType;
			uni.setStorageSync('loginType', loginType)
		},
		async setExpires(expiresIn : number) {
			this.expiresIn = expiresIn
			uni.setStorageSync('expiresIn', expiresIn)
		},

		async userLogout(state : IUser) {
			this.token = null
			this.info = null
			this.expiresIn = null
			this.username = ''
			this.password = ''
			uni.removeStorageSync('userInfo')
			uni.removeStorageSync('token')
			uni.removeStorageSync('expiresIn')
			uni.removeStorageSync('username')
			uni.removeStorageSync('password')

			//关闭socket
			uni.closeSocket({});

		}
	}
}))