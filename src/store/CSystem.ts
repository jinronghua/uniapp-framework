import { reactive } from 'vue'

let e = uni.getSystemInfoSync() as any

const rpxRatio = 750 / e.windowWidth
const statusBarHeight = e.statusBarHeight as number

let statusBar = 0 //状态栏高度
let customBar = 0 // 状态栏高度 + 导航栏高度

// #ifdef MP
statusBar = e.statusBarHeight
customBar = e.statusBarHeight + 45
if (e.platform === 'android') {
    customBar = e.statusBarHeight + 50
}
// #endif

// #ifdef MP-WEIXIN
statusBar = e.statusBarHeight
const custom = wx.getMenuButtonBoundingClientRect()
customBar = custom.bottom + custom.top - e.statusBarHeight

// #endif

// #ifdef MP-ALIPAY
statusBar = e.statusBarHeight
customBar = e.statusBarHeight + e.titleBarHeight
// #endif

// #ifdef APP-PLUS
console.log('app-plus', e)
statusBar = e.statusBarHeight
customBar = e.statusBarHeight + 45
// #endif

// #ifdef H5
statusBar = 0
customBar = e.statusBarHeight + 45
// #endif

const CSystem = reactive({
    /**
     * px和rpx换算比例
     */
    rpxRatio: rpxRatio,
    /**
     * 状态栏高度(px)
     */
    statusBarHeight: statusBar,
    statusBarHeightRpx: statusBarHeight * rpxRatio,
    /**
     * 自定义标题与胶囊对齐高度(px)
     */
    customBarHeight: customBar,
    customBarHeightRpx: customBar * rpxRatio,
    /**
     * 屏幕高度(px)
     */
    windowHeight: e.windowHeight,
    windowHeightRpx: e.windowHeight * rpxRatio,
    windowWidth: e.windowWidth,

    /*
    * 系统来源
    */
    platform: e.platform
})

export default CSystem
