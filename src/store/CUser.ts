import { reactive } from 'vue'

const CUser = reactive({
    info: {} as any,
    setInfo: (info: any) => {
        CUser.info = info
    },
    logout: () => {
        CUser.setInfo({})
    }
})
export default CUser
