## 1.0.2（2023-03-27）
- 升级请求签名验证模块 [uni-cloud-s2s](https://ext.dcloud.net.cn/plugin?name=uni-cloud-s2s) 至 1.0.1 版本
## 1.0.1（2023-03-02）
- 优化 实名认证页面逻辑
## 1.0.0（2023-02-27）
- 新增 实名认证云端一体模块 [详情](https://uniapp.dcloud.net.cn/uniCloud/frv/dev.html#uni-frv-external)
