module.exports = {
  getCertifyId: require('./getCertifyId'),
  getAuthResult: require('./getAuthResult'),
  _before: require('./_before'),
  _after: require('./_after')
}
