const sentence = {
  'uni-frv-invalid-param': 'Invalid parameter',
  'uni-frv-param-required': 'Parameter required: {param}',
  'uni-frv-fail': 'Real name certify failed',
  'uni-frv-frv-processing': 'Waiting for face recognition',
  'uni-frv-realname-verified': 'This account has been verified',
  'uni-frv-idcard-exists': 'The ID number has been bound to the account',
  'uni-frv-invalid-idcard': 'ID number is invalid',
  'uni-frv-invalid-realname': 'The name can only be Chinese characters',
  'uni-frv-unknown-error': 'unknown error',
  'uni-frv-realname-verify-upper-limit': 'The number of real-name certify on the day has reached the upper limit',
  'uni-frv-config-field-required': 'Config field required: {field}',
  'uni-frv-config-field-invalid': 'Config field: {field} is invalid',
  'uni-frv-certify-id-not-exist': 'certifyId does not exist',
  'uni-frv-certify-id-used': 'certifyId is used',
  'uni-id-illegal-request': 'Illegal request'
}

module.exports = sentence
