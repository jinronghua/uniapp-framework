const sentence = {
  'uni-frv-invalid-param': '参数错误',
  'uni-frv-param-required': '缺少参数: {param}',
  'uni-frv-fail': '实名认证失败',
  'uni-frv-frv-processing': '等待人脸识别',
  'uni-frv-realname-verified': '该账号已实名认证',
  'uni-frv-idcard-exists': '该证件号码已绑定账号',
  'uni-frv-invalid-idcard': '身份证号码不合法',
  'uni-frv-invalid-realname': '姓名只能是汉字',
  'uni-frv-unknown-error': '未知错误',
  'uni-frv-realname-verify-upper-limit': '当日实名认证次数已达上限',
  'uni-frv-config-field-required': '缺少配置项: {field}',
  'uni-frv-config-field-invalid': '配置项: {field}无效',
  'uni-frv-certify-id-not-exist': 'certifyId 不存在',
  'uni-frv-certify-id-used': 'certifyId 已使用',
  'uni-frv-illegal-request': '非法请求'
}

module.exports = sentence
