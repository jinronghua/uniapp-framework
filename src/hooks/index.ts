import { ref, onBeforeMount, reactive, getCurrentInstance } from 'vue'
import { onLaunch, onShow, onHide, onShareAppMessage, onShareTimeline } from "@dcloudio/uni-app";
import { componentsStore } from '@/store/modules/components'
import server from '@/apiData/api';
// const store = componentsStore()

const setNum = ref<number>(0);
/**
   @param {
    name:'getNotice',//在server里的接口名称
    title:'通知公告',//接口名，提示信息用
    noMsg:false,//是否不需要提示
    param:[],//参数，数组对象
    isRoot:false,//兼容接口没有返回code字段，直接返回数据的或跳过code判断需自己显示错误提示等情况(一般配合allBack:true使用)
    unLoading:Boolean，//是否显示loading效果
    allBack:false,//返回所有数据
  }
  */
export function setApiData() {
  const { proxy }: any = getCurrentInstance()
  const setApi = async (obj: any) => {

    let data: any = reactive({
      code: 500,
      msg: ''
    })
    try {
      if (!obj.unLoading) {
        // store.loadingFn_(true)
        setNum.value++
      }
      const param = obj.param || []
      const result = await server[obj.name](...param)
      if (result.code === 200 || obj.isRoot) {
        let code: any = {}
        code = {
          code: 200
        }
        data = {
          ...code,
          ...result
        }
      } else {
        console.error(obj.title + " 请求出错", result.code, result.msg)
        data = {
          msg: result.msg,
          code: 500
        }

      }

    } catch (e) {
      console.error(obj.title + " 请求失败", e + "", e)

      proxy.showMsg.show({ text: obj.title })
    }
    if (!obj.unLoading) {
      setNum.value--;
      if (setNum.value === 0) {
        // store.loadingFn_(false)
      }
    }
    return data;

  }
  return { setApi }
}
/**
 * @params {Function} fn  需要防抖的函数 delay 防抖时间
 * @returns {Function} debounce 防抖函数
 * @example  
 * const { debounce } = useDebounce()
 * const fn = () => { console.log('防抖') }
 * const debounceFn = debounce(fn, 1000)
 * debounceFn()
 */
export function useDebounce() {
  const debounce = (fn, delay) => {
    let timer = null;
    return function () {
      if (timer) clearTimeout(timer);
      timer = setTimeout(() => {
        fn.apply(this, arguments);
      }, delay);
    };
  };
  return { debounce };
}
/**
* @params {Function} fn  需要节流的函数 delay 节流时间
* @returns {Function} throttle 节流函数
* @example
* const { throttle } = useThrottle()
* const fn = () => { console.log('节流') }
* const throttleFn = throttle(fn, 1000)
* throttleFn()
*  */
export function useThrottle() {
  const throttle = (fn, delay) => {
    let timer = null;
    return function () {
      if (!timer) {
        timer = setTimeout(() => {
          fn.apply(this, arguments);
          timer = null;
        }, delay);
      }
    };
  };

  return { throttle };
}
/**
 *  倒计时
 *  @param {Number} second 倒计时秒数
 *  @return {Number} count 倒计时秒数
 *  @return {Function} countDown 倒计时函数
 *  @example
 *  const { count, countDown } = useCountDown()
 *  countDown(60)
 * <view>{{ count }}</view>
 */
export function useCountDown() {
  const count = ref(0);
  const timer = ref(null);
  const countDown = (second, ck) => {
    if (count.value === 0 && timer.value === null) {
      ck();
      count.value = second;
      timer.value = setInterval(() => {
        count.value--;
        if (count.value === 0) clearInterval(timer.value);
      }, 1000);
    }
  };
  onBeforeMount(() => {
    timer.value && clearInterval(timer.value);
  });
  return {
    count,
    countDown,
  };
}
/**
 * 获取随机字符串
 @param{
  
 }
 */
export function random_() {
  const randomValue: any = ref('')
  const randomString = (length) => {
    const randomStr = ref<string>("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
    for (var i = length; i > 0; --i) randomValue.value += randomStr.value[Math.floor(Math.random() * randomStr.value.length)];
  }
  return { randomString, randomValue }
}

export function Money() {
  const ObjectMoney = (Node) => {
    if (Node) {
      let opt = Node.split('.')
      if (opt.length == 1) {
        opt.push('00')
        return opt
      } else {
        return opt
      }
    } else {
      return ['0', '00']
    }
  }
  return { ObjectMoney }
}
export function sharefn() {
  const shareList = reactive({
    title: '开心桃小程序',
    path: '/pages/index/index',
    imageUrl: '',
    desc: '',
    content: ''
  })
  onShareAppMessage((res) => {
    console.log(res, 'res')
    console.log(shareList, 'share')
    return {
      title: shareList.title,
      path: shareList.path,
      imageUrl: shareList.imageUrl
    }
  })
  onShareTimeline(() => {
    return {
      title: shareList.title,
      path: '/pages/index/index',
      imageUrl: '',
      desc: '',
      content: ''
    }
    onShow((res) => {
      // uni.showShareMenu({
      //   withShareTicket: true,
      // })
    })
  })
  return {
    shareList
  }
}