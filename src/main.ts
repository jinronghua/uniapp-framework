import { createSSRApp } from 'vue';
import App from './App.vue';
import uviewPlus from 'uview-plus';
import { setupStore } from './store/index';
import directives from '@/directives/index';
// #ifndef MP-WEIXIN
import showMsg from '@/tsx/message';
// #endif
import useStore from '@/store/index2'
export function createApp() {
	const app = createSSRApp(App);
	app.use(uviewPlus);
	app.use(directives);
	setupStore(app);
	app.config.globalProperties.$store = useStore();
	// #ifndef MP-WEIXIN
	app.config.globalProperties['$msg'] = {
		show(msg : string, click : number) {
			showMsg(msg, click);
		},
	};
	// #endif
	return {
		app,
	};
}